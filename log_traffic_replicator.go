package main

import (
	"fmt"
	"github.com/hpcloud/tail"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"sync"
	"time"
)

const resultSize = 1000

type status struct {
	mu      sync.Mutex
	results [resultSize]struct {
		statuscode int
		url        string
		resptime   time.Duration
	}
	rps   [60]int
	index int
}

func (s *status) initStatus() {
	s.index = 0
	i := 0
	for i < resultSize {
		s.results[i].statuscode = 0
		i++
	}
	ticker := time.NewTicker(1 * time.Second)
	go func() {
		for _ = range ticker.C {
			j := time.Now().Second() + 4
			if j > 59 {
				j = j - 60
			}
			s.rps[j] = 0
		}
	}()

}

func (s *status) addResult(statuscode int, url string, resptime time.Duration) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.results[s.index].statuscode = statuscode
	s.results[s.index].url = url
	s.results[s.index].resptime = resptime
	s.index++
	s.rps[time.Now().Second()]++
	if s.index > resultSize-1 {
		s.index = 0
	}
}

func (s *status) respCodePCT() map[int]int {
	r := make(map[int]int)
	p := make(map[int]float64)
	var t float64
	for _, j := range s.results {
		if j.statuscode != 0 {
			p[j.statuscode]++
			t++
		}
	}
	for k, v := range p {
		r[k] = int(0.5 + v*100/t)
	}

	return r

}

func (s *status) printRespCodePCT() string {
	data := s.respCodePCT()
	var keys []int
	r := "[ "
	for k := range data {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for _, k := range keys {
		r = r + fmt.Sprintf("%v: %v, ", k, data[k])
	}
	r = r + "]"
	return r
}

func (s *status) get_rps() int {
	second := time.Now().Second() - 1
	if second < 0 {
		second = 59
	}
	return s.rps[second]
}

func (s *status) get_rpm() int {
	r := 0
	for _, j := range s.rps {
		r = r + j
	}
	return r
}

func (s *status) String() string {
	r := ""
	var t, i, max, min int64
	min = 99999999
	for i < resultSize && s.results[i].statuscode != 0 {
		j := s.results[i].resptime.Nanoseconds() / 1000000
		t = t + j
		i++
		if j < min {
			min = j
		}
		if j > max {
			max = j
		}
	}
	if i == 0 {
		r = "Starting\n"
	} else {
		r = fmt.Sprintf("Avg: %v ms\nMin: %v ms\nMax %v ms\nrps: %v\nrpm: %v\n", t/i, min, max, s.get_rps(), s.get_rpm())
		//r = r + fmt.Sprint(s.rps) + "\n"
		r = r + fmt.Sprintf("Response code pct: %v", s.printRespCodePCT()) + "\n"
	}
	return r
}

func parse(s string) string {
	regex := "(?P<remote>.+)\\s(?P<hostname>.+)\\s[\\-a-z]+\\s(.+)\\s\"(?P<req_type>[A-Z]+)\\s(.+)\\s.*\"\\s(?P<code>\\d+)\\s.+"
	re := regexp.MustCompile(regex)
	r := re.FindStringSubmatch(s)
	if len(r) > 4 {
		return r[5]
	} else {
		log.Printf(s)
		return ""
	}

}

func downloader(id string, wg *sync.WaitGroup, downloadQueue <-chan string, s *status) {
	defer wg.Done()
	for {
		select {
		case url, open := <-downloadQueue:
			if !open {
				return
			}
			start := time.Now()
			resp, err := http.Get(url)
			if err != nil {
				// handle error
				log.Fatal(err)
			}
			respcode := resp.StatusCode
			resp.Body.Close()
			elapsed := time.Since(start)
			if respcode != 200 {
				log.Printf("Error status %v %v %v", respcode, url, elapsed)
			}
			s.addResult(respcode, url, elapsed)
		}
	}
}

func tailer(maxWorkers int, downloadQueue chan string, downloadWg sync.WaitGroup, s *status, logfile string, newhost string) {
	seek := tail.SeekInfo{Offset: 0, Whence: 2}
	t, err := tail.TailFile(logfile, tail.Config{
		Location: &seek,
		Follow:   true,
		ReOpen:   true})
	if err != nil {
		log.Fatal(err)
	}
	for i := 0; i < cap(downloadQueue); i++ {
		downloadWg.Add(1)
		go downloader("downloader"+strconv.Itoa(i), &downloadWg, downloadQueue, s)
	}

	for line := range t.Lines {
		url := newhost + parse(line.Text)
		if url == "" {
			continue
		}
		if len(downloadQueue) == maxWorkers {
			log.Printf("DownloadQueueFull... Waiting")
		}
		downloadQueue <- url
	}
	downloadWg.Wait()

}

func printHelp() {
	fmt.Print("The program expect the following environment variables\n")
	fmt.Print("LTR_MAX_WORKERS: Max ammount of threads worker making request\n")
	fmt.Print("LTR_LOGFILE: filename pointing to the original traffic log\n")
	fmt.Print("LTR_NEWHOST: url to append to reach destination\n\n")
}

func getRunningParameters() (int, string, string) {
	maxWorkers, err := strconv.Atoi(os.Getenv("LTR_MAX_WORKERS"))
	if err != nil {
		printHelp()
		log.Fatal("Failed to parse LTR_MAX_WORKERS :", err)
	}
	logfile := os.Getenv("LTR_LOGFILE")
	if logfile == "" {
		printHelp()
		log.Fatal("Failed to parse LTR_LOGFILE :", err)
	}

	newhost := os.Getenv("LTR_NEWHOST")
	if newhost == "" {
		printHelp()
		log.Fatal("Failed to parse LTR_NEWHOST :", err)
	}

	return maxWorkers, logfile, newhost
}

func main() {
	maxWorkers, logfile, newhost := getRunningParameters()
	var s status
	downloadQueue := make(chan string, maxWorkers)
	var downloadWg sync.WaitGroup
	s.initStatus()
	go tailer(maxWorkers, downloadQueue, downloadWg, &s, logfile, newhost)

	http.HandleFunc("/stat", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, s.String())
	})
	http.HandleFunc("/dummy/", func(w http.ResponseWriter, r *http.Request) {
		//time.Sleep(time.Duration(rand.Intn(100)) * time.Millisecond )
		fmt.Fprintf(w, "For testing dummy")
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
