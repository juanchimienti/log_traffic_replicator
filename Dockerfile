from golang:1.10-alpine

run apk update; apk add git
WORKDIR /go/src/ltr
ADD log_traffic_replicator.go .
RUN go get -d -v ./...


RUN go install -v ./...

CMD ["ltr"]
