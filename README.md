# Log traffic replicator

Ltr is a program that parses a webserver logfile and extract the path of the url and then hits a different hostname in the same path as the lines are generated in the logfile. This is usefull to replicate traffic from one site to another.

## Usage

### Using the public image

```
docker  run --rm -ti -e LTR_MAX_WORKERS=10 -e LTR_LOGFILE=/data/access.log -e LTR_NEWHOST=http://other.example.com:8080 -v /var/log/nginx/:/data -p 8080:8080 registry.gitlab.com/juanchimienti/log_traffic_replicator
```

That way the file /var/log/nginx/access.log will be parsed and new request will be made to http://other.example.com:8080 by 10 workers at the rate that the lines appear on the file.

### Building and running.

#### Build

```
docker build -t ltr .

```

#### Run

Ltr expects 3 environment variables in order to work:

LTR_MAX_WORKERS: Max ammount of threads worker making requests

LTR_LOGFILE: filename pointing to the original traffic log

LTR_NEWHOST: url to append to reach destination

There are no defaults and if you fail to provide the variables the program wont start.

##### Stats

The program will listen to http request on port 8080 and publish the stats of the current run.

example

```
Avg: 150 ms
Min: 1 ms
Max 305 ms
rps: 7
rpm: 369
Response code pct: [ 200: 80, 404: 10, 503: 5, 500: 5, ]
```

The stats include:

Avg: ms average of response time from the LTR_NEWHOST.

Min: The amount of ms of the fastest response.

Max: The amount of ms of the slowest response.

rps: Request per second of the last second.

rpm: Request per minute of the last minute.

Response code pct: A list of the response code and the percentage of responses from LTR_NEWHOST for the last 1k request.




